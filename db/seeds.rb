# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).


#Create School
school = School.create(name: "Cool School")

#Create Gifts
Gift.create(name: "Cool Mug", gift_type: 'MUG', school: school)
Gift.create(name: "Cool Shirt", gift_type: "T_SHIRT", school: school)
Gift.create(name: "Cool Hoodie", gift_type: "HOODIE", school: school)
Gift.create(name: "Cool Sticker", gift_type: "STICKER", school: school)

#Create Recipients
Recipient.create(first_name: "John", last_name: "Wick", email: "john@example.com", phone: "7898767654", school: school)
Recipient.create(first_name: "Dexter", last_name: "Morgan", email: "dexter@example.com", phone: "7898767652", school: school)
