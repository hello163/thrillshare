class CreateOrdersRecipientsGifts < ActiveRecord::Migration[6.1]
  def change
    create_table :orders_recipients_gifts do |t|
      t.references :order, foreign_key: true
      t.references :gift, foreign_key: true
      t.references :recipient, foreign_key: true
      t.timestamps
    end
  end
end
