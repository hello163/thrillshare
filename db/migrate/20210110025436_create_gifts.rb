class CreateGifts < ActiveRecord::Migration[6.1]
  def change
    create_table :gifts do |t|
      t.string :name
      t.string :type
      t.references :school, foreign_key: true
      t.timestamps
    end
  end
end
