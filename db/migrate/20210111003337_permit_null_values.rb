class PermitNullValues < ActiveRecord::Migration[6.1]
  def change
    change_column :orders_recipients_gifts, :gift_id,:integer, null: true
    change_column :orders_recipients_gifts, :recipient_id, :integer, null: true
    change_column :orders_recipients_gifts, :order_id, :integer, null: true
  end
end
