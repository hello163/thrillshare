class CreateOrdersGifts < ActiveRecord::Migration[6.1]
  def change
    create_table :orders_gifts do |t|
      t.references :order
      t.references :gift
      t.timestamps
    end
  end
end
