class CreateOrdersRecipients < ActiveRecord::Migration[6.1]
  def change
    create_table :orders_recipients do |t|
      t.references :order
      t.references :recipient
      t.timestamps
    end
  end
end
