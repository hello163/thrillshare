# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_11_005741) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "gifts", force: :cascade do |t|
    t.string "name"
    t.string "gift_type"
    t.bigint "school_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["school_id"], name: "index_gifts_on_school_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "status"
    t.bigint "school_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["school_id"], name: "index_orders_on_school_id"
  end

  create_table "orders_gifts", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "gift_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gift_id"], name: "index_orders_gifts_on_gift_id"
    t.index ["order_id"], name: "index_orders_gifts_on_order_id"
  end

  create_table "orders_recipients", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "recipient_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_orders_recipients_on_order_id"
    t.index ["recipient_id"], name: "index_orders_recipients_on_recipient_id"
  end

  create_table "orders_recipients_gifts", force: :cascade do |t|
    t.integer "order_id"
    t.integer "gift_id"
    t.integer "recipient_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gift_id"], name: "index_orders_recipients_gifts_on_gift_id"
    t.index ["order_id"], name: "index_orders_recipients_gifts_on_order_id"
    t.index ["recipient_id"], name: "index_orders_recipients_gifts_on_recipient_id"
  end

  create_table "recipients", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "email", null: false
    t.string "phone"
    t.bigint "school_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["school_id"], name: "index_recipients_on_school_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "gifts", "schools"
  add_foreign_key "orders", "schools"
  add_foreign_key "orders_recipients_gifts", "gifts"
  add_foreign_key "orders_recipients_gifts", "orders"
  add_foreign_key "orders_recipients_gifts", "recipients"
  add_foreign_key "recipients", "schools"
end
