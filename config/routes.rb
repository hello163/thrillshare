Rails.application.routes.draw do

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  namespace :api, constraints: { format: 'json' } do
    namespace :v1 do
      resources :schools, only: [:index, :create, :update, :destroy] do
        resources :recipients, only: [:index, :create, :update, :destroy]
        resources :orders, only: [:index, :create, :update, :cancel, :ship] do
          post 'cancel', to: 'orders#cancel'
          post 'ship', to: 'orders#ship'
        end
      end
    end
  end

end
