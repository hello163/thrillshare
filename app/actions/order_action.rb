class OrderAction

  def self.create(params = {}, school_id)
    order = Order.new(params)

    if SchoolQuery.new(school_id).total_gifts_sending_today > School::LIMIT_GIFTS_PER_DAY
      order.errors.add(:invalid,"Schools are limited to send out a total of 60 gifts per day.")
      return order
    end

    order.school_id = school_id
    order.status = 'ORDER_RECEIVED'
    order.save
    order
  end

  def self.update(order, params = {})
    if (order.status == 'ORDER_SHIPPED')
      order.errors.add(:invalid,"Order with status:ORDER_SHIPPED cant be edited")
      return false,order
    end
    return order.update(params),order
  end

  def self.cancel(order)
    if (order.status == 'ORDER_SHIPPED')
      order.errors.add(:invalid,"Order with status:ORDER_SHIPPED cant be canceled")
      return false,order
    end
    order.status = 'ORDER_CANCELLED'
    return order.save,order
  end

  def self.ship(order)
    if (order.status == 'ORDER_CANCELLED')
      order.errors.add(:invalid,"Order with status:ORDER_CANCELLED cant be canceled")
      return false,order
    end
    order.status = 'ORDER_SHIPPED'
    return order.save,order
  end

end