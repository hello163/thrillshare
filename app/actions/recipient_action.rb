class RecipientAction

  # Create record of recipient and assigns the school id
  def self.create(params = {}, school_id)
    recipient = Recipient.new(params)
    recipient.school_id = school_id
    recipient.save
    recipient
  end

  # Return if the record is updated and instance of record
  def self.update(recipient,params = {})
    return recipient.update(params), recipient
  end

  # Return if the record is deleted from database
  def self.delete(recipient)
    return recipient.delete
  end

end