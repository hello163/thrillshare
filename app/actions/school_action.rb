class SchoolAction

  def self.create(params = {})
    School.create(params)
  end

  def self.delete(school)
    return school.delete
  end

end