class OrdersRecipientsGift < ApplicationRecord

  # Database relationships
  belongs_to :order
  belongs_to :gift
  belongs_to :recipient

  validates :gift, presence: false
  validates :recipient, presence: false
  validates :order, presence: false
end
