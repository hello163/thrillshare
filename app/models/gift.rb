class Gift < ApplicationRecord

  TYPES = %w(MUG T_SHIRT HOODIE STICKER).freeze

  # Database relationships
  belongs_to :school

  # Validations
  validates :name, presence: true
  validates :gift_type, inclusion: { in: TYPES}

end
