module BaseLog
  def to_s
    "#{self.class.name}: #{attributes}"
  end
end