class School < ApplicationRecord
  include BaseLog

  LIMIT_GIFTS_PER_DAY = 60

  # Database Relationships
  has_many :orders

  # Validations
  validates :name, presence: true

end
