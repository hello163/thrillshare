class Recipient < ApplicationRecord
  include BaseLog

  # Database relationships
  belongs_to :school
  has_many :orders, through: :orders_recipients_gifts


  # Validations
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }

end
