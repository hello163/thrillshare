class Order < ApplicationRecord
  include BaseLog

  STATUES = %w(ORDER_RECEIVED ORDER_PROCESSING ORDER_SHIPPED ORDER_CANCELLED).freeze
  LIMIT_RECIPIENTS_BY_ORDER = 20

  # Database relationships
  belongs_to :school
  has_many :orders_gifts
  has_many :orders_recipients
  has_many :gifts, through: :orders_gifts
  has_many :recipients, through: :orders_recipients

  # Validations
  validates :status, inclusion: { in: STATUES}
  validates :recipient_ids, presence: true
  validates :gift_ids, presence: true

  before_save :check_if_valid_number_of_recipients
  before_save :check_if_valid_number_gifts

  private

  def check_if_valid_number_of_recipients
    if recipients.count > LIMIT_RECIPIENTS_BY_ORDER
      errors.add(:invalid, "Individual Orders are limited to a maximum of 20 Recipients")
    end
  end

  def check_if_valid_number_gifts
    if (recipients.count * gifts.count) > School::LIMIT_GIFTS_PER_DAY
      errors.add(:invalid, "Schools are limited to send out a total of 60 gifts per day")
    end
  end

end
