class RecipientQuery

  def initialize(school_id,relation = Recipient.all)
    @relation = relation
    @school_id = school_id
  end

  def all
    @relation.where(school_id: @school_id)
  end



end