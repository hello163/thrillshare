class SchoolQuery

  def initialize(school_id)
    @school = School.find_by(id:school_id)
  end

  def today_orders
    @school.orders.where("created_at >= ?", Time.zone.now.beginning_of_day)
  end

  def total_gifts_sending_today
    orders = today_orders.includes(:gifts).includes(:recipients)
    orders.inject(0) {|sum,n| sum + (n.recipients.count * n.gifts.count)}
  end
end