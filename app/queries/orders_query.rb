class OrdersQuery

  def initialize(school_id,relation = Order.all)
    @relation = relation
    @school_id = school_id
  end

  def all
    @relation.where(school_id: @school_id).includes(:gifts).includes(:recipients)
  end



end