class Api::V1::RecipientsController < ApplicationController

  before_action :authenticate_token
  before_action :get_recipient, only:[:update, :destroy ]

  def index
    logger.info "Get all recipients"
    @recipients = RecipientQuery.new(params[:school_id]).all
  end

  def create
    logger.info "Creating Recipient.."
    @recipient = RecipientAction.create(recipient_params, params[:school_id])
    if @recipient.persisted?
      logger.info "Created: #{@recipient.to_s}"
      render json: @recipient, status: :created
    else
      logger.info "Errors: #{@recipient.errors}"
      render json: @recipient.errors, status: :bad_request
    end
  end

  def update
    logger.info "Updating Recipient.."
    updated, @recipient = RecipientAction.update(@recipient, recipient_params)
    if updated
      logger.info "Updated: #{@recipient.to_s}"
      render json: @recipient, status: :ok
    else
      logger.info "Errors: #{@recipient.errors}"
      render json: @recipient.errors, status: :bad_request
    end
  end

  def destroy
    logger.info "Deleting Recipient.."
    deleted = RecipientAction.delete(@recipient)
    if deleted
      render json: deleted, status: :ok
    else
      logger.info "Errors: #{@recipient.errors}"
      render json: @recipient.errors, status: :bad_request
    end
  end

  private

  def recipient_params
    params.require(:recipient).permit(:first_name, :last_name, :phone, :email)
  end

  def get_recipient
    @recipient = Recipient.find_by!(id:params[:id],school_id:params[:school_id])
  end

end