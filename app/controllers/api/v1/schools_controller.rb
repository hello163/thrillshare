class Api::V1::SchoolsController < ApplicationController

  before_action :authenticate_token
  before_action :get_school, only:[:update, :destroy]

  def index
    logger.info "Get all schools"
    @schools = School.all
  end

  def create
    logger.info "Creating School.."
    @school = SchoolAction.create(schools_params)
    if @school.persisted?
      logger.info "Created: #{@school.to_s}"
      render json: @school, status: :created
    else
      logger.info "Errors: #{@school.errors}"
      render json: @school.errors, status: :bad_request
    end
  end

  def update
    logger.info "Updating School.."
    if @school.update! schools_params
      logger.info "Updated: #{@school.to_s}"
      render json: @school, status: :ok
    else
      logger.info "Errors: #{@school.errors}"
      render json: @school.errors, status: :bad_request
    end
  end

  def destroy
    logger.info "Deleting School.."
    if SchoolAction.delete(@school)
      logger.info "Delete: #{@school.to_s}"
      render json: @school, status: :ok
    else
      logger.info "Errors: #{@school.errors}"
      render json: @school.errors, status: :bad_request
    end
  end

  private

  def schools_params
    params.require(:school).permit(:name)
  end

  def get_school
    @school = School.find_by_id! params[:id]
  end

end
