class Api::V1::OrdersController < ApplicationController

  before_action :authenticate_token
  before_action :get_order, only:[:update, :cancel, :ship ]

  def index
    @orders = OrdersQuery.new(params[:school_id]).all
  end

  def create
    @order = OrderAction.create(order_params,params[:school_id])
    if @order.persisted?
      logger.info "Created: #{@order.to_s}"
      render :show, status: :created
    else
      logger.info "Errors: #{@order.errors}"
      render json: @order.errors, status: :bad_request
    end
  end

  def update
    logger.info "Updating Order.."
    updated, @order = OrderAction.update(@order, order_params)
    if updated
      logger.info "Updated: #{@order.to_s}"
      render :show, status: :created
    else
      logger.info "Errors: #{@order.errors}"
      render json: @order.errors, status: :bad_request
    end
  end

  def cancel
    logger.info "Cancel Order.."
    updated, @order = OrderAction.cancel(@order)
    if updated
      logger.info "Cancel: #{@order.to_s}"
      render :show, status: :created
    else
      logger.info "Errors: #{@order.errors}"
      render json: @order.errors, status: :bad_request
    end
  end

  def ship
    logger.info "Ship Order.."
    updated, @order = OrderAction.ship(@order)
    if updated
      logger.info "Ship: #{@order.to_s}"
      render :show, status: :created
    else
      logger.info "Errors: #{@order.errors}"
      render json: @order.errors, status: :bad_request
    end
  end

  private

  def order_params
    params.require(:order).permit(gift_ids:[],recipient_ids:[])
  end

  def get_order
    id = params[:id] ||= params[:order_id]
    @order = Order.find_by!(id:id ,school_id:params[:school_id])
  end


end