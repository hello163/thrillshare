class ApplicationController < ActionController::API

  AUTH_HEADER = 'Authorization'

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

  def render_not_found_response(exception)
    render json: { error: exception.message }, status: :not_found
  end

  def authenticate_token
    begin
      logger.info "Authenticating Token..."
      validate_token!
    rescue => e
      render json: { errors: [{ message: e.message}], data: {} }, status: 401
    end
  end

  private

  def validate_token!
    unless Rails.application.secrets.dig(:thrillshare_api_key) == auth_token
      raise StandardError.new "Invalid Token"
    end
  end

  def auth_token
    if request.headers[AUTH_HEADER].present?
      return request.headers[AUTH_HEADER]
    else
      raise StandardError.new "Missing #{AUTH_HEADER}"
    end
  end
end
