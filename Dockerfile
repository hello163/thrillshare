FROM ruby:2.7.2
RUN apt-get update
RUN apt-get install build-essential libpq-dev zip -y

RUN mkdir /app

WORKDIR /app

ADD Gemfile /app/Gemfile

ADD Gemfile.lock /app/Gemfile.lock
RUN gem install bundler
RUN bundle install
ADD . /app

# Clean up un-needed files:
RUN rm -rf .git/* .dockerignore Dockerfile tmp/cache/* tmp/pids/* log/* docker-compose.yml *.env .env oracle/* pdfs/*
