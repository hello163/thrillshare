require 'rails_helper'

RSpec.describe School, type: :model do
  describe 'validations' do
    subject { build(:school) }
    it { should validate_presence_of(:name) }
  end
end
