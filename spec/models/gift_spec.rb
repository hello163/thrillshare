require 'rails_helper'

RSpec.describe Gift, type: :model do
  describe 'validations' do
    subject { build(:gift) }
    it { should validate_presence_of(:name) }
    it { should validate_inclusion_of(:gift_type).in_array(Gift::TYPES) }
  end
end
