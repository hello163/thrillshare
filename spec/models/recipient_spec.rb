require 'rails_helper'

RSpec.describe Recipient, type: :model do
  describe 'validations' do
    subject { build(:recipient) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:email) }
  end
end
