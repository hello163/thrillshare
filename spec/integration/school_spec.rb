require 'swagger_helper'

describe 'Blogs API' do

  path '/schools' do

    post 'Creates a Shool' do
      tags 'School'
      consumes 'application/json'
      parameter name: :school, in: :body, schema: {
          type: :object,
          properties: {
              name: { type: :string }
          },
          required: [ 'name' ]
      }

      response '201', 'school created' do
        let(:school) { { id: '1', name: 'bar' } }
        run_test!
      end

      response '401', 'Unauthorized' do
      end
    end
  end

  path '/blogs/{id}' do

    get 'Retrieves a blog' do
      tags 'Blogs'
      produces 'application/json', 'application/xml'
      parameter name: :id, in: :path, type: :string

      response '200', 'blog found' do
        schema type: :object,
               properties: {
                   id: { type: :integer },
                   title: { type: :string },
                   content: { type: :string }
               },
               required: [ 'id', 'title', 'content' ]

        let(:id) { Blog.create(title: 'foo', content: 'bar').id }
        run_test!
      end

      response '404', 'blog not found' do
        let(:id) { 'invalid' }
        run_test!
      end

      response '406', 'unsupported accept header' do
        let(:'Accept') { 'application/foo' }
        run_test!
      end
    end
  end
end
