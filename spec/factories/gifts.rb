FactoryBot.define do
  factory :gift do
    name { Faker::Superhero.name }
    gift_type { Gift::TYPES.sample }
  end
end
