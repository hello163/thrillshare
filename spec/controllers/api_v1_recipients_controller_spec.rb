require "rails_helper"

RSpec.describe Api::V1::RecipientsController, :type => :controller do
  describe "GET index" do

    it "has a 200 status code" do
      get :index,params: { school_id: 1 },xhr: true
      expect(response.status).to eq(200)
    end

    it "assigns @recipients" do
      recipient = create(:recipient)
      get :index,params: { school_id: recipient.school_id },xhr: true
      expect(assigns(:recipients)).to eq([recipient])
    end

    it "Create Recipient has a 201 status code" do
      school = create(:school)
      post :create, params: {
          recipient: {
              first_name: Faker::Name.first_name,
              last_name: Faker::Name.last_name,
              email: Faker::Internet.email,
              phone: Faker::PhoneNumber.cell_phone
          },
          school_id: school.id
      }, xhr: true
      expect(response.status).to eq(201)
    end

    it "Create School has a 201 status code" do
      school = create(:school)
      put :update, params: { school: { :name => "Any Name" },id: school.id }, xhr: true
      expect(response.status).to eq(200)
    end

    it "Permit parameters" do
      params = {
          school: {
              name: 'John'
          }
      }
      should permit(:name).
          for(:create, params: params).
          on(:school)
    end
  end
end
