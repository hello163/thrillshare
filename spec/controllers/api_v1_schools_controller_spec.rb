require "rails_helper"

RSpec.describe Api::V1::SchoolsController, :type => :controller do
  describe "Controller" do

    it "recipients has a 200 status code" do
      get :index,xhr: true
      expect(response.status).to eq(200)
    end

    it "assigns @schools" do
      school = create(:school)
      get :index, xhr: true
      expect(assigns(:schools)).to eq([school])
    end

    it "Create School has a 201 status code" do
      post :create, params: { school: { name: "Any Name" } }, xhr: true
      expect(response.status).to eq(201)
    end

    it "Update School has a 200 status code" do
      school = create(:school)
      put :update, params: { school: { :name => "Any Name" },id: school.id }, xhr: true
      expect(response.status).to eq(200)
    end

    it "Permit parameters" do
      params = {
          school: {
              name: 'John'
         }
       }
      should permit(:name).
          for(:create, params: params).
          on(:school)
    end
  end
end